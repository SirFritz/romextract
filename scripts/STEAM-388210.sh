#!/bin/sh

# Extraction script for:
# Day of the Tentacle Remastered (tenta.cle)

# Outputs:
# - Day of the Tentacle (CD/English)
# - Maniac Mansion (V2/DOS/English)

romextract()
{
	# Maniac Mansion files have different offsets in Linux and Windows archives
	if [ "${FILE##*.}" = "exe" ]; then
		MM_OFFSET=2966178
	else
		MM_OFFSET=0
	fi

	if [ -f "$(dirname "$FILE")/tenta.cle" ]; then
		FILE="$(dirname "$FILE")/tenta.cle"
	else
		echo "Could not find tenta.cle"
		return 1
	fi

	echo "Extracting game files from tenta.cle ..."
	mkdir -p $SCRIPTID/maniac
	# tail for offset, head for game file size
	tail -c +1884073597 "$FILE" | head -c +7932 \
		> "$SCRIPTID/tentacle.000"
	tail -c +1884081529 "$FILE" | head -c +11358470 \
		> "$SCRIPTID/tentacle.001"
	tail -c +1615108258 "$FILE" | head -c +268965339 \
		> "$SCRIPTID/monster.sou"
	tail -c +$((2399982851+MM_OFFSET)) "$FILE" | head -c +1988 \
		> "$SCRIPTID/maniac/00.lfl"
	tail -c +$((2399984839+MM_OFFSET)) "$FILE" | head -c +20833 \
		> "$SCRIPTID/maniac/01.lfl"
	tail -c +$((2400005672+MM_OFFSET)) "$FILE" | head -c +5103 \
		> "$SCRIPTID/maniac/02.lfl"
	tail -c +$((2400010775+MM_OFFSET)) "$FILE" | head -c +11332 \
		> "$SCRIPTID/maniac/03.lfl"
	tail -c +$((2400022107+MM_OFFSET)) "$FILE" | head -c +23596 \
		> "$SCRIPTID/maniac/04.lfl"
	tail -c +$((2400045703+MM_OFFSET)) "$FILE" | head -c +13931 \
		> "$SCRIPTID/maniac/05.lfl"
	tail -c +$((2400059634+MM_OFFSET)) "$FILE" | head -c +13210 \
		> "$SCRIPTID/maniac/06.lfl"
	tail -c +$((2400072844+MM_OFFSET)) "$FILE" | head -c +9999 \
		> "$SCRIPTID/maniac/07.lfl"
	tail -c +$((2400082843+MM_OFFSET)) "$FILE" | head -c +12069 \
		> "$SCRIPTID/maniac/08.lfl"
	tail -c +$((2400094912+MM_OFFSET)) "$FILE" | head -c +6871 \
		> "$SCRIPTID/maniac/09.lfl"
	tail -c +$((2400101783+MM_OFFSET)) "$FILE" | head -c +9708 \
		> "$SCRIPTID/maniac/10.lfl"
	tail -c +$((2400111491+MM_OFFSET)) "$FILE" | head -c +12199 \
		> "$SCRIPTID/maniac/11.lfl"
	tail -c +$((2400123690+MM_OFFSET)) "$FILE" | head -c +21208 \
		> "$SCRIPTID/maniac/12.lfl"
	tail -c +$((2400144898+MM_OFFSET)) "$FILE" | head -c +16369 \
		> "$SCRIPTID/maniac/13.lfl"
	tail -c +$((2400161267+MM_OFFSET)) "$FILE" | head -c +4966 \
		> "$SCRIPTID/maniac/14.lfl"
	tail -c +$((2400166233+MM_OFFSET)) "$FILE" | head -c +5781 \
		> "$SCRIPTID/maniac/15.lfl"
	tail -c +$((2400172014+MM_OFFSET)) "$FILE" | head -c +12192 \
		> "$SCRIPTID/maniac/16.lfl"
	tail -c +$((2400184206+MM_OFFSET)) "$FILE" | head -c +10342 \
		> "$SCRIPTID/maniac/17.lfl"
	tail -c +$((2400194548+MM_OFFSET)) "$FILE" | head -c +11656 \
		> "$SCRIPTID/maniac/18.lfl"
	tail -c +$((2400206204+MM_OFFSET)) "$FILE" | head -c +11990 \
		> "$SCRIPTID/maniac/19.lfl"
	tail -c +$((2400218194+MM_OFFSET)) "$FILE" | head -c +9461 \
		> "$SCRIPTID/maniac/20.lfl"
	tail -c +$((2400227655+MM_OFFSET)) "$FILE" | head -c +6063 \
		> "$SCRIPTID/maniac/21.lfl"
	tail -c +$((2400233718+MM_OFFSET)) "$FILE" | head -c +7355 \
		> "$SCRIPTID/maniac/22.lfl"
	tail -c +$((2400241073+MM_OFFSET)) "$FILE" | head -c +3543 \
		> "$SCRIPTID/maniac/23.lfl"
	tail -c +$((2400244616+MM_OFFSET)) "$FILE" | head -c +9989 \
		> "$SCRIPTID/maniac/24.lfl"
	tail -c +$((2400254605+MM_OFFSET)) "$FILE" | head -c +7082 \
		> "$SCRIPTID/maniac/25.lfl"
	tail -c +$((2400261687+MM_OFFSET)) "$FILE" | head -c +17456 \
		> "$SCRIPTID/maniac/26.lfl"
	tail -c +$((2400279143+MM_OFFSET)) "$FILE" | head -c +14373 \
		> "$SCRIPTID/maniac/27.lfl"
	tail -c +$((2400293516+MM_OFFSET)) "$FILE" | head -c +4672 \
		> "$SCRIPTID/maniac/28.lfl"
	tail -c +$((2400298188+MM_OFFSET)) "$FILE" | head -c +22429 \
		> "$SCRIPTID/maniac/29.lfl"
	tail -c +$((2400320617+MM_OFFSET)) "$FILE" | head -c +24719 \
		> "$SCRIPTID/maniac/30.lfl"
	tail -c +$((2400345336+MM_OFFSET)) "$FILE" | head -c +15006 \
		> "$SCRIPTID/maniac/31.lfl"
	tail -c +$((2400360342+MM_OFFSET)) "$FILE" | head -c +7656 \
		> "$SCRIPTID/maniac/32.lfl"
	tail -c +$((2400367998+MM_OFFSET)) "$FILE" | head -c +11699 \
		> "$SCRIPTID/maniac/33.lfl"
	tail -c +$((2400379697+MM_OFFSET)) "$FILE" | head -c +4994 \
		> "$SCRIPTID/maniac/34.lfl"
	tail -c +$((2400384691+MM_OFFSET)) "$FILE" | head -c +10849 \
		> "$SCRIPTID/maniac/35.lfl"
	tail -c +$((2400395540+MM_OFFSET)) "$FILE" | head -c +7534 \
		> "$SCRIPTID/maniac/36.lfl"
	tail -c +$((2400403074+MM_OFFSET)) "$FILE" | head -c +17128 \
		> "$SCRIPTID/maniac/37.lfl"
	tail -c +$((2400420202+MM_OFFSET)) "$FILE" | head -c +19278 \
		> "$SCRIPTID/maniac/38.lfl"
	tail -c +$((2400439480+MM_OFFSET)) "$FILE" | head -c +2518 \
		> "$SCRIPTID/maniac/39.lfl"
	tail -c +$((2400441998+MM_OFFSET)) "$FILE" | head -c +5177 \
		> "$SCRIPTID/maniac/40.lfl"
	tail -c +$((2400447175+MM_OFFSET)) "$FILE" | head -c +5564 \
		> "$SCRIPTID/maniac/41.lfl"
	tail -c +$((2400452739+MM_OFFSET)) "$FILE" | head -c +6625 \
		> "$SCRIPTID/maniac/42.lfl"
	tail -c +$((2400459364+MM_OFFSET)) "$FILE" | head -c +3038 \
		> "$SCRIPTID/maniac/43.lfl"
	tail -c +$((2400462402+MM_OFFSET)) "$FILE" | head -c +25510 \
		> "$SCRIPTID/maniac/44.lfl"
	tail -c +$((2400487912+MM_OFFSET)) "$FILE" | head -c +16172 \
		> "$SCRIPTID/maniac/45.lfl"
	tail -c +$((2400504084+MM_OFFSET)) "$FILE" | head -c +4607 \
		> "$SCRIPTID/maniac/46.lfl"
	tail -c +$((2400508691+MM_OFFSET)) "$FILE" | head -c +14239 \
		> "$SCRIPTID/maniac/47.lfl"
	tail -c +$((2400522930+MM_OFFSET)) "$FILE" | head -c +4050 \
		> "$SCRIPTID/maniac/48.lfl"
	tail -c +$((2400526980+MM_OFFSET)) "$FILE" | head -c +4710 \
		> "$SCRIPTID/maniac/49.lfl"
	tail -c +$((2400531690+MM_OFFSET)) "$FILE" | head -c +6325 \
		> "$SCRIPTID/maniac/50.lfl"
	tail -c +$((2400538015+MM_OFFSET)) "$FILE" | head -c +8753 \
		> "$SCRIPTID/maniac/51.lfl"
	tail -c +$((2400546768+MM_OFFSET)) "$FILE" | head -c +4014 \
		> "$SCRIPTID/maniac/52.lfl"
	tail -c +$((2400550782+MM_OFFSET)) "$FILE" | head -c +12995 \
		> "$SCRIPTID/maniac/53.lfl"

	echo "Script $SCRIPTID.sh done"
}
