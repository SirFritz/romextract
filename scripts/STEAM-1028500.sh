#!/bin/sh

# Extraction script for:
# Nightshade (game)

# Outputs:
# - Nightshade (Unverified).nes

romextract()
{
	
	echo "Copying uncompressed ROM ..."
	cp "$(dirname "$FILE")/res/game" "$SCRIPTID/Nightshade (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
