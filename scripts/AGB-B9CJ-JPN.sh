#!/bin/sh

# Extraction script for:
# Kunio-kun Nekketsu Collection 3 (Japan).gba

# Outputs:
# - Downtown Special - Kunio-kun no Jidaigeki Da yo Zenin Shuugou! (Japan) (Kunio-kun Nekketsu Collection 3) (Unverified).nes
# - Ike Ike! Nekketsu Hockey-bu - Subette Koronde Dairantou (Japan) (Kunio-kun Nekketsu Collection 3) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +49469 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Downtown Special - Kunio-kun no Jidaigeki Da yo Zenin Shuugou! (Japan) (Kunio-kun Nekketsu Collection 3) (Unverified).nes"
	tail -c +311673 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Ike Ike! Nekketsu Hockey-bu - Subette Koronde Dairantou (Japan) (Kunio-kun Nekketsu Collection 3) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
