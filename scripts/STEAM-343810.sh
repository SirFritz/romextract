#!/bin/sh

# Extraction script for:
# The Uninvited: MacVenture Series (uninvited.exe)

# Outputs:
# - Uninvited.2mg
# - Uninvited.dsk

romextract()
{

	echo "Extracting game files from uninvited.exe ..."
	# tail for offset, head for game file size
	tail -c +630105 "$FILE" | head -c +819273 \
		> "$SCRIPTID/Uninvited.2mg"
	tail -c +1449385 "$FILE" | head -c +819200 \
		> "$SCRIPTID/Uninvited.dsk"

	echo "Script $SCRIPTID.sh done"
}
