#!/bin/sh

# Extraction script for:
# Atari Vault

# Outputs:
# - astdelux.zip (Asteroids Deluxe (rev 2)/MAME 2003-Plus)
# - asteroid.zip (Asteroids (rev 2)/MAME 2003-Plus)
# - bwidow.zip (Black Widow/MAME 2003-Plus)
# - gravitar.zip (Gravitar (version 3)/MAME 2003-Plus)
# - llander.zip (Lunar Lander (rev 2)/MAME 2003-Plus)
# - mhavoc.zip (Major Havoc (rev 3)/MAME 2003-Plus)
# - missile.zip (Missile Command (set 1)/MAME 2003-Plus)
# - redbaron.zip (Red Baron/MAME 2003-Plus)
# - tempest.zip (Tempest (rev 3)/MAME 2003-Plus)
# - 3-D Tic-Tac-Toe (USA).a26
# - Adventure (USA).a26
# - Air Sea Battle (Unverified).a26
# - Asteroids (Unverified).a26
# - Backgammon (USA).a26
# - Basic Math (USA).a26
# - Basketball (Unverified).a26
# - Black Jack (USA).a26
# - Bowling (USA).a26
# - Brain Games (USA).a26
# - Breakout ~ Breakaway IV (Japan, USA).a26
# - Canyon Bomber (USA).a26
# - Casino ~ Poker Plus (USA).a26
# - Centipede (Japan, USA).a26
# - Championship Soccer ~ Soccer (USA).a26
# - Circus Atari (Japan, USA).a26
# - Codebreaker (USA).a26
# - Combat Two (USA) (Proto).a26
# - Combat (Unverified).a26
# - Crystal Castles (USA).a26
# - Demons to Diamonds (USA).a26
# - Desert Falcon (USA).a26
# - Dodge 'Em ~ Dodger Cars (USA).a26
# - Double Dunk (Unverified).a26
# - Fatal Run (Unverified).a26
# - Flag Capture ~ Capture (USA).a26
# - Football (USA).a26
# - Golf (USA).a26
# - Gravitar (USA).a26
# - Hangman ~ Spelling (USA).a26
# - Haunted House (USA).a26
# - Home Run ~ Baseball (USA).a26
# - Human Cannonball ~ Cannon Man (USA).a26
# - Hunt & Score ~ Memory Match (USA).a26
# - Indy 500 ~ Race (USA).a26
# - Maze Craze - A Game of Cops 'n Robbers ~ Maze Mania - A Game of Cops 'n Robbers (USA).a26
# - Millipede (USA).a26
# - Miniature Golf ~ Arcade Golf (USA).a26
# - Missile Command (USA).a26
# - Night Driver (USA).a26
# - Off the Wall (Unverified).a26
# - Outlaw ~ Gunslinger (USA).a26
# - Quadrun (Unverified).a26
# - Radar Lock (USA).a26
# - RealSports Baseball (USA).a26
# - RealSports Basketball (Europe) (Proto).a26
# - RealSports Boxing (USA).a26
# - RealSports Football (USA).a26
# - RealSports Soccer (USA).a26
# - RealSports Tennis (USA).a26
# - RealSports Volleyball (USA).a26
# - Return to Haunted House (Unverified).a26
# - Save Mary! (USA) (Proto).a26
# - Secret Quest (USA).a26
# - Sentinel (Unverified).a26
# - Sky Diver ~ Dare Diver (USA).a26
# - Slot Machine ~ Slots (USA).a26
# - Slot Racers ~ Maze (USA).a26
# - Space War ~ Space Combat (USA).a26
# - Sprint Master (USA).a26
# - Star Raiders (USA).a26
# - Star Ship ~ Outer Space (USA).a26
# - Steeplechase (USA).a26
# - Stellar Track (USA).a26
# - Street Racer ~ Speedway II (USA).a26
# - Stunt Cycle (USA) (Proto).a26
# - Submarine Commander (USA).a26
# - Super Baseball (Unverified).a26
# - Super Breakout (Unverified).a26
# - Super Football (USA).a26
# - Surround ~ Chase (USA).a26
# - SwordQuest - Earthworld (Unverified).a26
# - SwordQuest - FireWorld (USA).a26
# - SwordQuest - WaterWorld (USA).a26
# - Tempest (Unverified).a26
# - Video Checkers ~ Checkers (USA).a26
# - Video Chess (USA).a26
# - Video Cube (Unverified).a26
# - Video Olympics ~ Pong Sports (USA).a26
# - Video Pinball ~ Arcade Pinball (USA).a26
# - Warlords (USA).a26
# - Yars' Revenge (USA).a26

## With Atari Vault - 50 Game Add-On Pack DLC
# - Adventure II (Proto) (Unverified).a26
# - Air Raiders (USA).a26
# - Aquaventure (USA) (Proto) (1983-08-12).a26
# - Armor Ambush (USA).a26
# - Astroblast (USA) (v1.1).a26
# - Dark Cavern (USA).a26
# - Frog Pond (USA) (Proto).a26
# - Frogs and Flies (USA).a26
# - Holey Moley (USA) (Proto).a26
# - International Soccer (USA).a26
# - MotoRodeo (USA).a26
# - Saboteur (Proto) (Unverified).a26
# - Sea Battle (USA).a26
# - Space Attack (USA).a26
# - Star Strike (USA).a26
# - Super Challenge Baseball (USA).a26
# - Super Challenge Football (USA).a26
# - Swordfight (USA).a26
# - Wizard (Proto) (Unverified).a26
# - Yars' Return (Proto) (Unverified).a26
# - [BIOS] Atari 5200 (USA).a52
# - Asteroids (USA) (Proto).a52
# - Centipede (USA).a52
# - Final Legacy (USA) (Proto).a52
# - Microgammon SB (USA) (Proto).a52
# - Millipede (USA) (Proto).a52
# - Miniature Golf (USA) (Proto).a52
# - Missile Command (USA).a52
# - RealSports Baseball (USA).a52
# - RealSports Basketball (USA).a52
# - RealSports Football (USA).a52
# - RealSports Soccer (USA).a52
# - RealSports Tennis (USA).a52
# - Star Raiders (USA).a52
# - Super Breakout (USA).a52
# - Xari Arena (USA) (Proto).a52

romextract()
{
	DIR="$(dirname "$FILE")/AtariVault_Data/StreamingAssets/FOCAL_Emulator"

	echo "Extracting and compressing MAME 2003-Plus ROMs ..."
	# TODO: check if tiles, sprites and sounds can be convert back into a MAME compatible ROM

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux" || return 1

	# tail for offset, head for ROM size
	tail -c +1 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036430.02"
	tail -c +2049 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036431.02"
	tail -c +4097 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036432.02"
	tail -c +6145 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036433.03"
	tail -c +8193 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036800.02"
	tail -c +10241 "$DIR/Asteroids Deluxe.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/036799.01"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/astdelux/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid" || return 1

	tail -c +1 "$DIR/Asteroids.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid/035145.02"
	tail -c +2049 "$DIR/Asteroids.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid/035144.02"
	tail -c +4097 "$DIR/Asteroids.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid/035143.02"
	tail -c +6145 "$DIR/Asteroids.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid/035127.02"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/asteroid/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow" || return 1

	tail -c +1 "$DIR/Black Widow.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.107"
	tail -c +2049 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.108"
	tail -c +6145 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.109"
	tail -c +10241 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.110"
	tail -c +14337 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.101"
	tail -c +18433 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.102"
	tail -c +22529 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.103"
	tail -c +26625 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.104"
	tail -c +30721 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.105"
	tail -c +34817 "$DIR/Black Widow.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.106"

	# change bytes 0xa05 and 0xe03 from 0x05 to 0x85 to match MAME ROM
	# octal for Dash/POSIX compatibility
	printf '\205' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.106" count=1 bs=1 seek=2565 conv=notrunc
	printf '\205' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/136017.106" count=1 bs=1 seek=3587 conv=notrunc

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/bwidow/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar" || return 1

	tail -c +1 "$DIR/Gravitar.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.210"
	tail -c +2049 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.207"
	tail -c +6145 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.208"
	tail -c +10241 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.309"
	tail -c +14337 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.301"
	tail -c +18433 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.302"
	tail -c +22529 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.303"
	tail -c +26625 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.304"
	tail -c +30721 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.305"
	tail -c +34817 "$DIR/Gravitar.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/136010.306"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/gravitar/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander" || return 1

	tail -c +1 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034599.01"
	tail -c +2049 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034598.01"
	tail -c +6145 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034572.02"
	tail -c +8193 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034571.02"
	tail -c +10241 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034570.01"
	tail -c +12289 "$DIR/Lunar Lander.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/034569.02"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/llander/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc" || return 1

	tail -c +1 "$DIR/Major Havoc.bin" | head -c +4096 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.210"
	tail -c +4097 "$DIR/Major Havoc.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.216"
	tail -c +20481 "$DIR/Major Havoc.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.217"
	tail -c +1 "$DIR/Major Havoc alpha banks.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.215"
	tail -c +16385 "$DIR/Major Havoc alpha banks.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.318"
	tail -c +1 "$DIR/Major Havoc vector banks.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.106"
	tail -c +16385 "$DIR/Major Havoc vector banks.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.107"
	tail -c +1 "$DIR/Major Havoc gamma.bin" | head -c +16384 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.108"
	truncate -s8192 "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/136025.210"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/mhavoc/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile" || return 1

	tail -c +1 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035820.02"
	tail -c +2049 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035821.02"
	tail -c +4097 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035822.02"
	tail -c +6145 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035823.02"
	tail -c +8193 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035824.02"
	tail -c +10241 "$DIR/Missile Command.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/035825.02"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/missile/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron" || return 1

	tail -c +1 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/037587.01"
	tail -c +2049 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/037000.01e"
	tail -c +4097 "$DIR/Red Baron.bin" | head -c +2048 \
		>> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/037587.01"
	tail -c +6145 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/036998.01e"
	tail -c +8193 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/036997.01e"
	tail -c +10241 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/036996.01e"
	tail -c +12289 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/036995.01e"
	tail -c +14337 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/037006.01e"
	tail -c +16385 "$DIR/Red Baron.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/037007.01e"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/redbaron/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest" || return 1

	tail -c +1 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.113"
	tail -c +2049 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.114"
	tail -c +4097 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.115"
	tail -c +6145 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.316"
	tail -c +8193 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.217"
	tail -c +10241 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.118"
	tail -c +12289 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.119"
	tail -c +14337 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.120"
	tail -c +16385 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.121"
	tail -c +18433 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.222"
	tail -c +20481 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.123"
	tail -c +22529 "$DIR/Tempest.bin" | head -c +2048 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/136002.124"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/tempest/"* > /dev/null

	echo "Copy MAME 2003-Plus ROMs ..."

	mkdir -p "$SCRIPTID" || return 1

	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/"*.zip "$SCRIPTID/"

	echo "Copy Atari 2600 ROMs ..."

	cp "$DIR/3d_tic.bin" "$SCRIPTID/3-D Tic-Tac-Toe (USA).a26"
	cp "$DIR/advnture.bin" "$SCRIPTID/Adventure (USA).a26"
	cp "$DIR/airseabt.bin" "$SCRIPTID/Air Sea Battle (Unverified).a26"
	cp "$DIR/asteroid.bin" "$SCRIPTID/Asteroids (Unverified).a26"
	cp "$DIR/backgamn.bin" "$SCRIPTID/Backgammon (USA).a26"
	cp "$DIR/basketbl.bin" "$SCRIPTID/Basketball (Unverified).a26"
	cp "$DIR/basmath.bin" "$SCRIPTID/Basic Math (USA).a26"
	cp "$DIR/black_j.bin" "$SCRIPTID/Black Jack (USA).a26"
	cp "$DIR/bowling.bin" "$SCRIPTID/Bowling (USA).a26"
	cp "$DIR/braingms.bin" "$SCRIPTID/Brain Games (USA).a26"
	cp "$DIR/breakout.bin" "$SCRIPTID/Breakout ~ Breakaway IV (Japan, USA).a26"
	cp "$DIR/canyonb.bin" "$SCRIPTID/Canyon Bomber (USA).a26"
	cp "$DIR/casino.bin" "$SCRIPTID/Casino ~ Poker Plus (USA).a26"
	cp "$DIR/centiped.bin" "$SCRIPTID/Centipede (Japan, USA).a26"
	cp "$DIR/circatri.bin" "$SCRIPTID/Circus Atari (Japan, USA).a26"
	cp "$DIR/codebrk.bin" "$SCRIPTID/Codebreaker (USA).a26"
	cp "$DIR/combat2.bin" "$SCRIPTID/Combat Two (USA) (Proto).a26"
	cp "$DIR/combat.bin" "$SCRIPTID/Combat (Unverified).a26"
	cp "$DIR/concentr.bin" "$SCRIPTID/Hunt & Score ~ Memory Match (USA).a26"
	cp "$DIR/cryscast.bin" "$SCRIPTID/Crystal Castles (USA).a26"
	cp "$DIR/demondim.bin" "$SCRIPTID/Demons to Diamonds (USA).a26"
	cp "$DIR/dodge_em.bin" "$SCRIPTID/Dodge 'Em ~ Dodger Cars (USA).a26"
	cp "$DIR/doubdunk.bin" "$SCRIPTID/Double Dunk (Unverified).a26"
	cp "$DIR/dsrtfalc.bin" "$SCRIPTID/Desert Falcon (USA).a26"
	cp "$DIR/fatalrun.bin" "$SCRIPTID/Fatal Run (Unverified).a26"
	cp "$DIR/flagcap.bin" "$SCRIPTID/Flag Capture ~ Capture (USA).a26"
	cp "$DIR/football.bin" "$SCRIPTID/Football (USA).a26"
	cp "$DIR/golf.bin" "$SCRIPTID/Golf (USA).a26"
	cp "$DIR/grav2600.bin" "$SCRIPTID/Gravitar (USA).a26"
	cp "$DIR/hangman.bin" "$SCRIPTID/Hangman ~ Spelling (USA).a26"
	cp "$DIR/haunthse.bin" "$SCRIPTID/Haunted House (USA).a26"
	cp "$DIR/homerun.bin" "$SCRIPTID/Home Run ~ Baseball (USA).a26"
	cp "$DIR/human_cb.bin" "$SCRIPTID/Human Cannonball ~ Cannon Man (USA).a26"
	cp "$DIR/indy500.bin" "$SCRIPTID/Indy 500 ~ Race (USA).a26"
	cp "$DIR/mazecrz.bin" "$SCRIPTID/Maze Craze - A Game of Cops 'n Robbers ~ Maze Mania - A Game of Cops 'n Robbers (USA).a26"
	cp "$DIR/milliped.bin" "$SCRIPTID/Millipede (USA).a26"
	cp "$DIR/min_golf.bin" "$SCRIPTID/Miniature Golf ~ Arcade Golf (USA).a26"
	cp "$DIR/misscomm.bin" "$SCRIPTID/Missile Command (USA).a26"
	cp "$DIR/nightdrv.bin" "$SCRIPTID/Night Driver (USA).a26"
	cp "$DIR/ofthwall.bin" "$SCRIPTID/Off the Wall (Unverified).a26"
	cp "$DIR/outlaw.bin" "$SCRIPTID/Outlaw ~ Gunslinger (USA).a26"
	cp "$DIR/pelesocr.bin" "$SCRIPTID/Championship Soccer ~ Soccer (USA).a26"
	cp "$DIR/quadrun.bin" "$SCRIPTID/Quadrun (Unverified).a26"
	cp "$DIR/radarlok.bin" "$SCRIPTID/Radar Lock (USA).a26"
	cp "$DIR/ret2hh.bin" "$SCRIPTID/Return to Haunted House (Unverified).a26"
	cp "$DIR/rs_baseb.bin" "$SCRIPTID/RealSports Baseball (USA).a26"
	cp "$DIR/rsbasket.bin" "$SCRIPTID/RealSports Basketball (Europe) (Proto).a26"
	cp "$DIR/rsboxing.bin" "$SCRIPTID/RealSports Boxing (USA).a26"
	cp "$DIR/rs_footb.bin" "$SCRIPTID/RealSports Football (USA).a26"
	cp "$DIR/rssoccer.bin" "$SCRIPTID/RealSports Soccer (USA).a26"
	cp "$DIR/rstennis.bin" "$SCRIPTID/RealSports Tennis (USA).a26"
	cp "$DIR/rs_volly.bin" "$SCRIPTID/RealSports Volleyball (USA).a26"
	cp "$DIR/SaveMary_NTSC.bin" "$SCRIPTID/Save Mary! (USA) (Proto).a26"
	cp "$DIR/secretq.bin" "$SCRIPTID/Secret Quest (USA).a26"
	cp "$DIR/sentinel.bin" "$SCRIPTID/Sentinel (Unverified).a26"
	cp "$DIR/skydiver.bin" "$SCRIPTID/Sky Diver ~ Dare Diver (USA).a26"
	cp "$DIR/slotmach.bin" "$SCRIPTID/Slot Machine ~ Slots (USA).a26"
	cp "$DIR/slotrace.bin" "$SCRIPTID/Slot Racers ~ Maze (USA).a26"
	cp "$DIR/spacewar.bin" "$SCRIPTID/Space War ~ Space Combat (USA).a26"
	cp "$DIR/sprbaseb.bin" "$SCRIPTID/Super Baseball (Unverified).a26"
	cp "$DIR/sprfootb.bin" "$SCRIPTID/Super Football (USA).a26"
	cp "$DIR/sprntmas.bin" "$SCRIPTID/Sprint Master (USA).a26"
	cp "$DIR/sq_earth.bin" "$SCRIPTID/SwordQuest - Earthworld (Unverified).a26"
	cp "$DIR/sq_fire.bin" "$SCRIPTID/SwordQuest - FireWorld (USA).a26"
	cp "$DIR/sq_water.bin" "$SCRIPTID/SwordQuest - WaterWorld (USA).a26"
	cp "$DIR/starraid.bin" "$SCRIPTID/Star Raiders (USA).a26"
	cp "$DIR/starship.bin" "$SCRIPTID/Star Ship ~ Outer Space (USA).a26"
	cp "$DIR/steplchs.bin" "$SCRIPTID/Steeplechase (USA).a26"
	cp "$DIR/stlrtrak.bin" "$SCRIPTID/Stellar Track (USA).a26"
	cp "$DIR/streetrc.bin" "$SCRIPTID/Street Racer ~ Speedway II (USA).a26"
	cp "$DIR/stunt.bin" "$SCRIPTID/Stunt Cycle (USA) (Proto).a26"
	cp "$DIR/subcmdr.bin" "$SCRIPTID/Submarine Commander (USA).a26"
	cp "$DIR/superbrk.bin" "$SCRIPTID/Super Breakout (Unverified).a26"
	cp "$DIR/surround.bin" "$SCRIPTID/Surround ~ Chase (USA).a26"
	cp "$DIR/temp2600.bin" "$SCRIPTID/Tempest (Unverified).a26"
	cp "$DIR/vidcheck.bin" "$SCRIPTID/Video Checkers ~ Checkers (USA).a26"
	cp "$DIR/vidchess.bin" "$SCRIPTID/Video Chess (USA).a26"
	cp "$DIR/vidcube.bin" "$SCRIPTID/Video Cube (Unverified).a26"
	cp "$DIR/vid_olym.bin" "$SCRIPTID/Video Olympics ~ Pong Sports (USA).a26"
	cp "$DIR/vidpin.bin" "$SCRIPTID/Video Pinball ~ Arcade Pinball (USA).a26"
	cp "$DIR/warl2600.bin" "$SCRIPTID/Warlords (USA).a26"
	cp "$DIR/yar_rev.bin" "$SCRIPTID/Yars' Revenge (USA).a26"

	if [ -d "$DIR/vol3" ]; then
		cp "$DIR/vol3/adventureii.bin" "$SCRIPTID/Adventure II (Proto) (Unverified).a26"
		cp "$DIR/vol3/airaidrs.bin" "$SCRIPTID/Air Raiders (USA).a26"
		cp "$DIR/vol3/aquavent.bin" "$SCRIPTID/Aquaventure (USA) (Proto) (1983-08-12).a26"
		cp "$DIR/vol3/armambsh.bin" "$SCRIPTID/Armor Ambush (USA).a26"
		cp "$DIR/vol3/astrblst.bin" "$SCRIPTID/Astroblast (USA) (v1.1).a26"
		cp "$DIR/vol3/darkcvrn.bin" "$SCRIPTID/Dark Cavern (USA).a26"
		cp "$DIR/vol3/frog_pond_8_27_82.bin" "$SCRIPTID/Frog Pond (USA) (Proto).a26"
		cp "$DIR/vol3/frogflys.bin" "$SCRIPTID/Frogs and Flies (USA).a26"
		cp "$DIR/vol3/holemole.bin" "$SCRIPTID/Holey Moley (USA) (Proto).a26"
		cp "$DIR/vol3/pele_tw.bin" "$SCRIPTID/International Soccer (USA).a26"
		cp "$DIR/vol3/motorodeo_ntsc.bin" "$SCRIPTID/MotoRodeo (USA).a26"
		cp "$DIR/vol3/saboteur.bin" "$SCRIPTID/Saboteur (Proto) (Unverified).a26"
		cp "$DIR/vol3/seabattle.bin" "$SCRIPTID/Sea Battle (USA).a26"
		cp "$DIR/vol3/spacattk.bin" "$SCRIPTID/Space Attack (USA).a26"
		cp "$DIR/vol3/starstrk.bin" "$SCRIPTID/Star Strike (USA).a26"
		cp "$DIR/vol3/suprbase.bin" "$SCRIPTID/Super Challenge Baseball (USA).a26"
		cp "$DIR/vol3/suprfoot.bin" "$SCRIPTID/Super Challenge Football (USA).a26"
		cp "$DIR/vol3/swordfight.bin" "$SCRIPTID/Swordfight (USA).a26"
		cp "$DIR/vol3/wizard.bin" "$SCRIPTID/Wizard (Proto) (Unverified).a26"
		cp "$DIR/vol3/yarsreturn.bin" "$SCRIPTID/Yars' Return (Proto) (Unverified).a26"
	fi

	if [ -d "$DIR/5200" ]; then
		echo "Copy Atari 5200 ROMs ..."

		cp "$DIR/5200/bios.bin" "$SCRIPTID/[BIOS] Atari 5200 (USA).a52"
		cp "$DIR/5200/asteroids.bin" "$SCRIPTID/Asteroids (USA) (Proto).a52"
		cp "$DIR/5200/centipede.bin" "$SCRIPTID/Centipede (USA).a52"
		cp "$DIR/5200/final_legacy.bin" "$SCRIPTID/Final Legacy (USA) (Proto).a52"
		cp "$DIR/5200/microgammon.bin" "$SCRIPTID/Microgammon SB (USA) (Proto).a52"
		cp "$DIR/5200/millipede.bin" "$SCRIPTID/Millipede (USA) (Proto).a52"
		cp "$DIR/5200/miniature_golf.bin" "$SCRIPTID/Miniature Golf (USA) (Proto).a52"
		cp "$DIR/5200/missile_command.bin" "$SCRIPTID/Missile Command (USA).a52"
		cp "$DIR/5200/realsports_baseball.bin" "$SCRIPTID/RealSports Baseball (USA).a52"
		cp "$DIR/5200/realsports_basketball.bin" "$SCRIPTID/RealSports Basketball (USA).a52"
		cp "$DIR/5200/realsports_football.bin" "$SCRIPTID/RealSports Football (USA).a52"
		cp "$DIR/5200/realsports_soccer.bin" "$SCRIPTID/RealSports Soccer (USA).a52"
		cp "$DIR/5200/realsports_tennis.bin" "$SCRIPTID/RealSports Tennis (USA).a52"
		cp "$DIR/5200/star_raiders.bin" "$SCRIPTID/Star Raiders (USA).a52"
		cp "$DIR/5200/super_breakout.bin" "$SCRIPTID/Super Breakout (USA).a52"
		cp "$DIR/5200/xari_arena.bin" "$SCRIPTID/Xari Arena (USA) (Proto).a52"
	fi


	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"

}
