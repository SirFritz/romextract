#!/bin/sh

# Extraction script for:
# Kunio-kun Nekketsu Collection 2 (Japan).gba

# Outputs:
# - Downtown - Nekketsu Koushinkyoku - Soreyuke Daiundoukai (Japan) (Kunio-kun Nekketsu Collection 2) (Unverified).nes
# - Nekketsu Koukou Dodgeball-bu - Soccer Hen (Japan) (Kunio-kun Nekketsu Collection 2) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +49453 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Downtown - Nekketsu Koushinkyoku - Soreyuke Daiundoukai (Japan) (Kunio-kun Nekketsu Collection 2) (Unverified).nes"
	tail -c +311657 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Nekketsu Koukou Dodgeball-bu - Soccer Hen (Japan) (Kunio-kun Nekketsu Collection 2) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
