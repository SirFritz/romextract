#!/bin/sh

# Extraction script for:
# Contra 4 (USA).nds

# Outputs:
# - Contra (USA).nes
# - Super C (USA).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +28492801 "$FILE" | head -c +131088 \
		> "$SCRIPTID/Contra (USA).nes"
	tail -c +29973505 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Super C (USA).nes"

	echo "Script $SCRIPTID.sh done"
}