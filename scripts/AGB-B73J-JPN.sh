#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 3 - Action Collection (Japan).gba

# Outputs:
# - Challenger (Japan) (Hudson Best Collection) (Unverified).nes
# - Meikyuu Kumikyoku - Milon no Daibouken (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +164145 "$FILE" | head -c +40976 \
		> "$SCRIPTID/Challenger (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +205121 "$FILE" | head -c +65552 \
		> "$SCRIPTID/Meikyuu Kumikyoku - Milon no Daibouken (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
