#!/bin/sh

# Extraction script for:
# Kunio-kun Nekketsu Collection 1 (Japan).gba

# Outputs:
# - Nekketsu Koukou Dodgeball-bu (Japan) (Kunio-kun Nekketsu Collection 1) (Unverified).nes
# - Nekketsu! Street Basket - Ganbare Dunk Heroes (Japan) (Kunio-kun Nekketsu Collection 1) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +49453 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Nekketsu Koukou Dodgeball-bu (Japan) (Kunio-kun Nekketsu Collection 1) (Unverified).nes"
	tail -c +311657 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Nekketsu! Street Basket - Ganbare Dunk Heroes (Japan) (Kunio-kun Nekketsu Collection 1) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
