#!/bin/sh

# Extraction script for:
# Super Smash Bros. Brawl (Europe) (En,Fr,De,Es,It) (v1.01).iso

# Outputs:
# - Donkey Kong (World) (Rev A).nes
# - Kirby's Adventure (Europe).nes
# - Kirby's Adventure (France).nes
# - Legend of Zelda, The (E) (VC).nes
# - Legend of Zelda, The - Ocarina of Time (Europe) (En,Fr,De) (Rev A).z64
# - Lylat Wars (Europe) (En,Fr,De).z64
# - Super Mario Bros. (Europe) (Rev A).nes

# Requires: wit python2 brrencode3.py snesrestore.py ucon64

romextract()
{
	dependency_wit          || return 1
	dependency_python2      || return 1
	dependency_snesrestore  || return 1
	dependency_ucon64       || return 1

	# Extract only VC partitions (PTAB1)
	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" --psel=PTAB1 \
		--files=+/files/rom \
		--files=+/sys/main.dol \
		--files=+/files/content5

	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +1102097 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HA8P/sys/main.dol" \
		| head -c +24592 > "$SCRIPTID/Donkey Kong (World) (Rev A).nes"
	tail -c +1102097 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HA9P/sys/main.dol" \
		| head -c +40976 > "$SCRIPTID/Super Mario Bros. (Europe) (Rev A).nes"
	tail -c +1124689 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBAP/sys/main.dol" \
		| head -c +131088 > "$SCRIPTID/Legend of Zelda, The (E) (VC).nes"
	tail -c +1116113 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBBF/sys/main.dol" \
		| head -c +786448 > "$SCRIPTID/Kirby's Adventure (France).nes"
	tail -c +1118193 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBBP/sys/main.dol" \
		| head -c +786448 > "$SCRIPTID/Kirby's Adventure (Europe).nes"
	tail -c +1125745 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBCP/sys/main.dol" \
		| head -c +131088 > "$SCRIPTID/Kid Icarus (Unverified).nes" # TODO: Unverified
	tail -c +1102097 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBDP/sys/main.dol" \
		| head -c +24592 > "$SCRIPTID/Ice Climber (Unverified).nes" # TODO: Unverified
	tail -c +1117553 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBEP/sys/main.dol" \
		| head -c +262160 > "$SCRIPTID/Super Mario Bros. 2 (Unverified).nes" # TODO: Unverified

	echo "Moving files ..."
	#SNES
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBFP/files/content5/JAAP.rom" \
		"$SCRIPTID/Super Mario World (Virtual Console) (Unverified).rom"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBGP/files/content5/JACP.rom" \
		"$SCRIPTID/F-Zero (Virtual Console) (Unverified).rom"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBIP/files/content5/JAVP.rom" \
		"$SCRIPTID/Super Metroid (Virtual Console) (Unverified).rom"
	# N64
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBKP/files/rom" \
		"$SCRIPTID/Legend of Zelda, The - Ocarina of Time (Europe) (En,Fr,De) (Rev A).z64"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBLP/files/rom" \
		"$SCRIPTID/Lylat Wars (Europe) (En,Fr,De).z64"

	# TODO: GoodTools hashes match files without uCON64 chk fix
	echo "Merging Super Mario World ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/Super Mario World (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBFP/files/content5/JAAP.pcm" \
		"$SCRIPTID/Super Mario World (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for Super Mario World ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/Super Mario World (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1
	echo "Merging F-Zero ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/F-Zero (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBGP/files/content5/JACP.pcm" \
		"$SCRIPTID/F-Zero (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for F-Zero ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/F-Zero (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1
	echo "Merging Super Metroid ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/Super Metroid (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/P-HBIP/files/content5/JAVP.pcm" \
		"$SCRIPTID/Super Metroid (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for Super Metroid ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/Super Metroid (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
