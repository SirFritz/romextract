#!/bin/sh

# Extraction script for:
# Cinemaware Anthology: 1986-1991 (Anthology.exe)

# Outputs:
# - Kickstart v1.2 rev 33.180
# - Defender of the Crown
# - It Came from the Desert
# - It Came from the Desert II - Antheads
# - The King of Chicago
# - Lords of the Rising Sun
# - Rocket Ranger
# - Rocket Ranger Censored Version
# - SDI
# - Sinbad and the Throne of the Falcon
# - TV Sports Baseball
# - TV Sports Basketball
# - TV Sports Boxing
# - TV Sports Football
# - Wings

romextract()
{

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID" || return 1

	echo "Extracting files from Anthology.exe ..."
	# tail for offset, head for game file size
	tail -c +445041 "$FILE" | head -c +19945104 \
		> "$ROMEXTRACT_TMPDIR/$SCRIPTID/Anthology.zip"

	echo "Decompressing files ..."
	unzip -uo "$ROMEXTRACT_TMPDIR/$SCRIPTID/Anthology.zip" \
		-d "$ROMEXTRACT_TMPDIR/$SCRIPTID" > /dev/null

	echo "Moving files ..."
	# Kickstart 1.2
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/os/Kick12.rom" \
		"$SCRIPTID/Kickstart v1.2 rev 33.180 (1986)(Commodore)(A500-A1000-A2000).rom"
	# Rocket Ranger
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskrre1.adf" \
		"$SCRIPTID/Rocket Ranger (1988)(Cinemaware)(Disk 1 of 2).adf"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskrre2.adf" \
		"$SCRIPTID/Rocket Ranger (1988)(Cinemaware)(Disk 2 of 2).adf"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskrrg1.adf" \
		"$SCRIPTID/Rocket Ranger (1988)(Cinemaware)(De)(en)(Disk 1 of 2).adf"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskrrg2.adf" \
		"$SCRIPTID/Rocket Ranger (1988)(Cinemaware)(De)(en)(Disk 2 of 2).adf"
	# The King of Chicago
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskkoc1.adf" \
		"$SCRIPTID/King of Chicago, The (1987)(Cinemaware)(Disk 1 of 2).adf"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/floppy/diskkoc2.adf" \
		"$SCRIPTID/King of Chicago, The (1987)(Cinemaware)(Disk 2 of 2).adf"
	# tail for offset, head for game file size
	# Defender of the Crown
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdefender.bin" | head -c +901120 \
				> "$SCRIPTID/Defender of the Crown (1986)(Cinemaware)(Disk 1 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdefender.bin" | head -c +901120 \
		> "$SCRIPTID/Defender of the Crown (1986)(Cinemaware)(Disk 2 of 2).adf"
	# It Came from the Desert
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert (1989)(Cinemaware)(Disk 1 of 3).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert (1989)(Cinemaware)(Disk 2 of 3).adf"
	tail -c +2703365 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert (1989)(Cinemaware)(Disk 3 of 3).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert (1989)(Cinemaware)(Savedisk).adf"
	# It Came from the Desert II - Antheads
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert2.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert II - Antheads (1990)(Mirrorsoft)(Disk 1 of 3).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert2.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert II - Antheads (1990)(Mirrorsoft)(Disk 2 of 3).adf"
	tail -c +2703365 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert2.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert II - Antheads (1990)(Mirrorsoft)(Disk 3 of 3).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romdesert2.bin" | head -c +901120 \
		> "$SCRIPTID/It Came from the Desert II - Antheads (1990)(Mirrorsoft)(Savedisk).adf"
	# Lords of the Rising Sun
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romlords.bin" | head -c +901120 \
		> "$SCRIPTID/Lords of the Rising Sun (1989)(Cinemaware)(Disk 1 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romlords.bin" | head -c +901120 \
		> "$SCRIPTID/Lords of the Rising Sun (1989)(Cinemaware)(Disk 2 of 2).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romlords.bin" | head -c +901120 \
		> "$SCRIPTID/Lords of the Rising Sun (1989)(Cinemaware)(Savedisk).adf"
	# SDI
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romsdi.bin" | head -c +901120 \
		> "$SCRIPTID/SDI (1987)(Cinemaware).adf"
	# Sinbad and the Throne of the Falcon
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romsinbad.bin" | head -c +901120 \
		> "$SCRIPTID/Sinbad and the Throne of the Falcon (1987)(Cinemaware)(Disk 1 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romsinbad.bin" | head -c +901120 \
		> "$SCRIPTID/Sinbad and the Throne of the Falcon (1987)(Cinemaware)(Disk 2 of 2).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romsinbad.bin" | head -c +901120 \
		> "$SCRIPTID/Sinbad and the Throne of the Falcon (1987)(Cinemaware)(Savedisk).adf"
	# TV Sports Baseball
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombaseball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Baseball (1992)(Mindscape)(Disk 1 of 2).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombaseball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Baseball (1992)(Mindscape)(Disk 2 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombaseball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Baseball (1992)(Mindscape)(Savedisk).adf"
	# TV Sports Basketball
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombasketball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Basketball (1990)(Cinemaware)(Disk 1 of 2).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombasketball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Basketball (1990)(Cinemaware)(Disk 2 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/rombasketball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Basketball (1990)(Cinemaware)(Savedisk).adf"
	# TV Sports Boxing
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romboxing.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Boxing (1991)(Mindscape)(Disk 1 of 2).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romboxing.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Boxing (1991)(Mindscape)(Disk 2 of 2).adf"
	# TV Sports Football
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romfootball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Football (1988)(Cinemaware)(Disk 1 of 3).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romfootball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Football (1988)(Cinemaware)(Disk 2 of 3).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romfootball.bin" | head -c +901120 \
		> "$SCRIPTID/TV Sports Football (1988)(Cinemaware)(Disk 3 of 3).adf"
	# Wings
	tail -c +5 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romwings.bin" | head -c +901120 \
		> "$SCRIPTID/Wings (1990)(Cinemaware)(Disk 1 of 3).adf"
	tail -c +901125 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romwings.bin" | head -c +901120 \
		> "$SCRIPTID/Wings (1990)(Cinemaware)(Disk 2 of 3).adf"
	tail -c +1802245 "$ROMEXTRACT_TMPDIR/$SCRIPTID/packaged/romdisk/romwings.bin" | head -c +901120 \
		> "$SCRIPTID/Wings (1990)(Cinemaware)(Disk 3 of 3).adf"

	echo "Cleaning up ..."
  chmod 0640 "$SCRIPTID/"*
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
